# coding: utf-8

# Angelo Victor Kraemer Foletto
## Num USP: 12620258
### SCC5830— Prof. Moacir A. Ponti
### Assignment 2: enhancement and superresolution
# ---

import numpy as np
import imageio as imgio
import math


class Module02():

    def __init__(self, imgs, imghigh, method, param,  size_matrix=256):
        self.imgs = imgs
        self.imghigh = imghigh
        self.method = method
        self.param = param
        self.size_matrix = size_matrix

    def __createMatrix(self):
        """
        Create new matrix in 0 values.
        """
        return np.zeros(self.size_matrix, dtype=np.float64)

    def __accumulated(self, matrix):
        """
        Accumulated values.
        """
        for i in range(1, self.size_matrix):
            matrix[i] += matrix[i - 1]
        return matrix

    def __histogram(self, img):
        """
        Find histogram accumulated in the image.
        """
        matrix = self.__createMatrix()
        for i in range(img.shape[0]):
            for j in range(img.shape[1]):
                matrix[int(img[i, j])] += 1
        return self.__accumulated(matrix)

    def __histogramWithList(self, list):
        """
        Calculate the accumulated histogram of the images in 'ArrayList'.
        """
        matrix = self.__createMatrix()

        for i in range(len(list)):  # Scrolls through images.
            for j in range(list[i].shape[0]):
                for k in range(list[i].shape[1]):
                    matrix[int(list[i][j, k])] += 1
        return self.__accumulated(matrix)

    def __equalise(self, img, histogram, count):
        """
        Equalizes the 'Histogram' histogram.
        """
        size_color = 255
        ratio = float(size_color / (math.pow(self.size_matrix, 2) * count))
        return np.float64(np.multiply(histogram[img], ratio))

    def __gamma(self, img):
        """
        Aply gamma correction in image
        """
        size_color = 255
        return np.floor(np.multiply(size_color, np.power(np.divide(img, float(size_color)), self.param)))

    def __calcRMSE(self, img_highes, load_img):
        """
        Calc RMSE
        """
        pw = 2
        return math.sqrt(
            np.sum(np.power(np.subtract(load_img, img_highes), pw))
            / (load_img.shape[0] * load_img.shape[1])
        )

    def __generateImgHighRes(self, img_highes, imgs, size_up):
        """
        Generates high resolution images.
        """
        for i in range(imgs[0].shape[0]):
            for j in range(imgs[0].shape[1]):
                img_highes[i * size_up, j * size_up]         = np.float64(imgs[0][i, j])
                img_highes[i * size_up, j * size_up + 1]     = np.float64(imgs[1][i, j])
                img_highes[i * size_up + 1, j * size_up]     = np.float64(imgs[2][i, j])
                img_highes[i * size_up + 1, j * size_up + 1] = np.float64(imgs[3][i, j])
        return img_highes

    def __clearCache(self):
        """
        Clear a dyrt cache.
        """
        del self.imgs

    def __method(self):
        """
        Process highlighting
        """
        if self.method == 1:
            for i in range(4):
                histogram_swap = self.__histogram(self.imgs[i])
                self.imgs[i] = self.__equalise(self.imgs[i], histogram_swap, 1)
        elif self.method == 2:
            histogram_swap = self.__histogramWithList(self.imgs)
            for i in range(4):
                self.imgs[i] = self.__equalise(self.imgs[i], histogram_swap, 4)
        elif self.method == 3:
            for i in range(4):
                self.imgs[i] = self.__gamma(self.imgs[i])
        else:
            return

    def main(self):
        """
        The main
        """
        self.__method()

        # Allocate high resolution image
        size_up = 2  # up 2x size image
        new_size = self.size_matrix * size_up

        return self.__calcRMSE(
            imgio.imread(self.imghigh).astype(np.uint8) # H
            , self.__generateImgHighRes(np.empty([new_size, new_size], dtype=np.float64)
                                        , self.imgs
                                        , size_up
                                        ).astype(np.uint8)  # Ĥ
        )


if __name__ == "__main__":
    # Input
    imglow = str(input()).rstrip()
    imghigh = str(input()).rstrip()
    method = int(input())
    param = float(input())

    mod = Module02([imgio.imread(imglow + '0.png'),
                    imgio.imread(imglow + '1.png'),
                    imgio.imread(imglow + '2.png'),
                    imgio.imread(imglow + '3.png')]  # Loading images
                   , imghigh
                   , method
                   , param
                   )

    print('%.4f' % round(mod.main(), 4))
